---
title:      Feeds : Historique
date:       2023-08-03
updated:    2024-12-07
---

°°changelog°°
0.6.2 øø(2024-12-07)øø:
    • fix: localization regression
0.6.1 øø(2024-12-07)øø:
    • fix: keepalive usage
0.6.0 øø(2024-04-17)øø:
    • add: bookmarks
    • fix: parse xml regression
0.5.0 øø(2024-02-25)øø:
    • add: localisation
    • fix: [#1](https://framagit.org/rui.nibau/feeds/-/issues/1) articles order
0.4.1 øø(2024-02-04)øø:
    • fix: styles
0.4.0 øø(2024-02-04)øø:
    • upd: code & doc
    • add: css user
0.3.0 øø(2023-12-09)øø:
    • upd: build generates bundle
    • fix: mobile/tablet display
    • fix: run service as user
0.2.0 øø(2023-10-04)øø:
    • upd: Première version publique.
0.1.0 øø(2023-08-03)øø:
    • add: première version.


