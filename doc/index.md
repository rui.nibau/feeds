---
title:      Feeds
banner:     /lab/feeds/pics/banner.png
cover:      /lab/feeds/pics/cover.png
date:       2023-08-03
updated:    2024-12-07
cats:       [informatique]
tags:       [rss, apps]
techs:      [javascript, nodejs, deno]
itemtype:   WebApplication
source:     https://framagit.org/rui.nibau/feeds
issues:     https://framagit.org/rui.nibau/feeds/-/issues
download:   /lab/feeds/build/latest/feeds.zip
version:    0.6.2
intro: Application web qui permet de lire des flux rss/atom et de gérer une liste de flux au format opml. Elle tourne sur node ou sur Deno.
status: in-process
---

## Notes

..include::./notes.md

## Documentation

..include::./docs.md

°°list-style-stack°°
• [Historique](/projets/feeds/changelog)
• [Licence](/projets/feeds/licence)

