---
title: Feeds : notes
date: 2023-08-03
updated: 2024-04-18
cats: [informatique]
tags: [rss, apps]
techs: [javascript, nodejs, deno]
intro: Notes sur le développement de l'application Feeds.
---

°°list-style-stack°°
• [øø2024-04-18øø : Feeds 0.6.0 et arrivée des marque-pages](/projets/feeds/notes/2024-04-18)


