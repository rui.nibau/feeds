---
title:      Feeds : Licence
date:       2023-08-03
updated:    2023-09-18
---

[GPL-3.0-or-later](https://www.gnu.org/licenses/gpl-3.0.en.html) :

Feeds - A simple Feed reader

Copyright (C) 2023-2024 Rui Nibau

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

