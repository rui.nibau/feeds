---
title: Feeds : documentation
date: 2023-08-03
updated: 2024-04-18
cats: [informatique]
tags: [rss, apps]
techs: [javascript, nodejs, deno]
intro: Documentation de l'application Feeds.
---

°°list-style-stack°°
• [Présentation](/projets/feeds/docs/about)
• [Installation](/projets/feeds/docs/install)
• [Configuration](/projets/feeds/docs/config)
• [utilisation](/projets/feeds/docs/usage)
• [Hack](/projets/feeds/docs/hack)


