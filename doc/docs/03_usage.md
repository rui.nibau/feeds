---
title:  Feeds : utilisation
date:   2023-08-03
intro:  Utilisation de l'application.
---

°°todo°°En cours de rédaction...

°°|new-feed°°
## Ajouter un nouveau flux

• Cliquer sur le bouton « + »
• Renseigner les champs « name » et « url »

°°pic pos-centre sz-full°°
![Capture d'écran](/lab/feeds/pics/0.2.0-create-feed.png)

°°|new-folder°°
## Ajouter un outline

• Cliquer sur le bouton « + »
• Renseigner uniquement le champ « name »

## Réorganiser les outlines

• Déplacer un outline par simple glisser/déposer dans l'arborescence
• Déposer un outline sur un outline de flux l'insérera avant cet outline de flux
• Déposer un outline sur un outline parent en fera un nouvel enfant

## Éditer un outline

°°pic pos-centre sz-full°°
![Capture d'écran](/lab/feeds/pics/0.2.0-edit-outline.png)

°°todo°°À écrire...


