---
title:  Feeds : présentation
date:   2023-08-03
intro:  Présentation de l'application feeds.
---

°°pic pos-centre sz-full°°
![Capture d'écran](/lab/feeds/pics/0.2.0.png)


## pourquoi Feeds

Je lis des flux [RSS](https://fr.wikipedia.org/wiki/RSS) ou [Atom](https://fr.wikipedia.org/wiki/Atom_Syndication_Format) sans doute depuis que je traîne sur les internets, soit 2003.

Je l'ai toujours fait à l'aide d'une extension de navigateur web, pour Mozilla web d'abord, [Firefox](/articles/firefox#addons-doc) ensuite. je n'ai jamais réussi à utiliser des applications dédiées comme l'antique [RSSOwl](http://www.rssowl.org/) ou le plus récent [Gnome Feeds](https://gfeeds.gabmus.org/).

J'ai utilisé [Brief](https://addons.mozilla.org/fr/firefox/addon/brief/) pendant de (très) nombreuses années, jusqu'au jour où j'ai voulu dupliqué mon profil Firefox et que je me suis rendu compte que **la base de donnée SQLite de Brief pesait plus de 600 Mo** ! 600 Mo alors que l'application n'est sensé stocker que peu de choses.

C'est [un bug connu](https://github.com/brief-rss/brief/issues/194), donné comme corrigé mais cet incident m'a poussé à sortir des cartons un projet commencé il y a quelques années, sans doute en 2019 : mon propre lecteur de flux rss.

**''Feeds''** est donc une simple application web entièrement écrite en javascript et capable de lire des flux rss. --Elle n'est pas destinée à stocker des données, juste à consulter une liste de flux et à afficher les articles non lus-- (Lire « [Arrivée des marque-pages](/projets/feeds/notes/2024-04-18) »).

Son développement est en cours, il y a des bugs et elle ne gère absolument pas tous les types de flux rss / atom qui existent. Mais je l'utilise quotidiennement depuis plusieurs mois pour lire une centaine de flux.

## Fonctionnalités

• Liste de flux rss/atom sous forme de fichier opml.
• Ajout/suppression/édition de flux / outline.
• Réorganisation des outlines par glisser/déposer.
• Marquer des articles de flux comme lu.
• Lancer la mise à jour des flux ou d'un flux particulier.
• Forcer le recharegement d'un flux.
• Interface traduite (anglais, français)

## Métriques, performances

• Lecture de 120 flux rss/atom en ~ 15s. Ça peut être amélioré.
• Données de 120 flux rss/atom stockés dans un fichier opml de 23 ko et un fichier d'état de 300 ko, là où Brief occupait 600 Mo pour un même nombre de flux. Et ça peut encore être amélioré.


