---
title:  Feeds : hack
date:   2023-08-03
intro:  Jouer avec les sources de l'application.
---

## Dépendances de dev

Dépendances « internes »:
    • [rnb-dev-web](https://framagit.org/rui.nibau/rnb-dev-web) : configurations
Dépendances externes:
    • [eslint](https://eslint.org/) et [eslint-plugin-jsdoc](https://github.com/gajus/eslint-plugin-jsdoc) : validation du code
    • [clean-css](https://github.com/clean-css/clean-css) : fusion et nettoyage des fichiers css

## Dépendances de l'application

Librairies tiers utilisées par l'application, les « dépendances internes » étant des dépendances à des outils que je développe moi-même.

Dependances « internes »:
    • [rnb-data](/projets/rnb-data) : internationalisation
    • [rnb-http](/projets/rnb-http) : serveur http.
    • [rnb-logs](/projets/rnb-logs) : logs serveur.
    • [rnb-runtime](/projets/rnb-runtime) : proxy pour le runtime javascript.
    • [rnb-ui](/projets/rnb-ui) : interface graphique
Dépendances externes:
    • [@ungap/custom-elements](https://github.com/ungap/custom-elements) : permettre à l'application de tourner sous Safari.

