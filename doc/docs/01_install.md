---
title:  Feeds : installation
date:   2023-08-03
intro:  Installation de Feeds. L'application nécessite [nodejs](https://nodejs.org) ≥ 18.17 ou [Deno](https://deno.com) ≥ 1.36.
---

[Télécharger l'application](/lab/feeds/build/latest/feeds.zip) et dézipper le contenu de l'archive à l'endroit où vous souhaitez faire tourner l'application. 

## Lancement en ligne de commande

Avec NodeJs :

°°stx-bash°°
    node /path/to/feeds/src/feeds.js start

Avec Deno :

°°stx-bash°°
    deno run --allow-net --allow-write --allow-read /path/to/feeds/src/feeds.js start

Go to ``http://localhost:8002``.

## Déployer en tant que service

Create a systemd service :

----
    [Unit]
    Description=Feeds service
    After=network.target
    
    [Service]
    Type=oneshot
    ExecStart=node /path/to/feeds/src/feeds.js start
    
    [Install]
    WantedBy=default.target
----
``~/.config/systemd/user/comics.service``

Reload systemd :

    systemctl --user daemon-reload

Enable the service :

    systemctl --user enable feeds.service

Start the service :

°°stx-bash°°
    systemctl --user start feeds.service

Go to ``http://localhost:8002``.

