---
title:  Feeds : Configuration
date:   2023-08-03
intro:  Configuration de l'application
---

## config.json

L'application peut être configurer à l'aide d'un fichier ``config.json`` à placer dans le dossier ``data``.

▾ **server**: (otpional)
    • **port**: ''number'' — Server port (8002 by default)
    • **logs**: 
        • **level**: ''string'' — Log level ('debug'|'info'|'error'). 'error' by default
▾ **ssl**: (otpional) To activate https
    • **key**: ''string'' — path to key file for ssl (can be relative)
    • **cert**: ''string'' — path to cert file for ssl (can be relative)
▾ **deploy**: (optional) (dev)
    • **host**: ''string'' — Host where to deploy
    • **path**: ''string'' — Path on host where to deploy

°°stx-js°°
    {
        "server": {
            "port": 8002,
            "logs": {
                "level": "error"
            }
        },
        "ssl": {
            "key": "feeds.key",
            "cert": "feeds.cert"
        },
        "deploy": {
            "host": "user@ip",
            "path": "/path/to/app/deployement/"
        }
    }
Exemple de fichier ``config.json``

## feeds.opml

Vous pouvez placer un fichier [opml](https://fr.wikipedia.org/wiki/Outline_Processor_Markup_Language) nommer « feeds.opml » dans le dossier « data ». S'il ce fichier est absent, il sera créé lors du premier ajout de flux.

°°warn°°
ATTENTION : J'utilise l'application quotidiennement depuis øøplusieurs mois|2023-10øø sans problèmes. Néanmoins, des pertes de données sont toujours possibles donc : Faites une SAUVEGARDE de votre liste de flux régulièrement.


