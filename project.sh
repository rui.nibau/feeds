#!/bin/bash

set -e

DIR="$( cd "$( dirname "$0" )" && pwd )"

cd $DIR

package=$(< ./package.json)

# vars
VER=$(echo "$package" | jq -r .version)
NAME=$(echo "$package" | jq -r .name)
ENTRY="main.js"

# dirs
SRC_DIR=$DIR"/src/"
BUILD_DIR=$DIR"/build/"$VER
BUILD_DIR_APP=$BUILD_DIR"/"$NAME
DATA_DIR=$DIR"/data/"

# build
build() {
    echo -e "Building "$NAME" "$VER"..."

    # clean
    echo "  > cleaning directory..."
    if [[ -d $BUILD_DIR ]]; then
        rm -fr $BUILD_DIR
    fi
    if [[ -d $DIR"/build/latest" ]]; then
        rm -fr $DIR"/build/latest"
    fi

    # build
    mkdir $BUILD_DIR

    # make dir
    cd $BUILD_DIR
    mkdir $NAME
    mkdir $NAME"/data"

    echo "  > Copying files..."

    mkdir -p $NAME"/src/frontend"

    # copy
    cp -prL -t $BUILD_DIR_APP $DIR"/feeds.service" $DIR"/package.json" $DIR"/start.sh"
    cp -prL $SRC_DIR"/feeds.js" $BUILD_DIR_APP"/src/feeds.js"
    cp -prL $SRC_DIR"/frontend/_locales" $BUILD_DIR_APP"/src/frontend/_locales"
    cp -prL $SRC_DIR"/frontend/thirdparty" $BUILD_DIR_APP"/src/frontend/thirdparty"
    cp -prL $SRC_DIR"/frontend/index.html" $BUILD_DIR_APP"/src/frontend/index.html"
    cp -prL $SRC_DIR"/frontend/favicon.ico" $BUILD_DIR_APP"/src/frontend/favicon.ico"
    cp -prL $SRC_DIR"/frontend/favicon.png" $BUILD_DIR_APP"/src/frontend/favicon.png"
    
    cd $DIR

    # build js
    esbuild --bundle --minify $SRC_DIR"/frontend/feeds/main.js" --outfile=$BUILD_DIR_APP"/src/frontend/feeds/main.js"
    cp -prL $SRC_DIR"/server/" $BUILD_DIR_APP"/src/server/"

    # Build css
    esbuild --bundle --minify $SRC_DIR"/frontend/feeds/styles.css" --outfile=$BUILD_DIR_APP"/src/frontend/feeds/styles.css"

    # create zip
    echo "  > Zipping archive..."
    cd $BUILD_DIR
    zip -qr $NAME".zip" $NAME

    # create latest dir
    cd ../
    cp -prL $BUILD_DIR $DIR"/build/latest/"

    echo -e "Build done."
}

deploy() {
    build

    cd $DIR
    local config=$(< ./data/config.json)
    local host=$(echo "$config" | jq -r .deploy.host)
    local path=$(echo "$config" | jq -r .deploy.path)

    if [[ $host && $path ]]; then
        echo -e "Deploying "$NAME" "$VER" to "$host":"$path"..."
        rsync -qcrLktzh --exclude 'feeds.service' --exclude 'data/*' --delete --force --ignore-errors --progress $BUILD_DIR"/"$NAME"/" $host":"$path
        if [[ $1 == "restart" ]]; then
            echo -e "restarting "$NAME" "$VER" at "$host"..."
            ssh $host systemctl --user restart $NAME".service" &
            sleep 3
            ssh $host systemctl --user status $NAME".service" &
        fi
    else
        echo -e "[ERROR] No host and/or path to deploy."
    fi
    echo -e "Deploy done."
}

setup() {
    echo "Setup..."
    echo "symlinks..."
    echo "setup done."
}

deps() {
    # FIXME Do do it if folder already exists
    echo -e "Cloning dependencies id deps directory..."
    cd $DIR
    # clear
    rm -fr deps/*/
    # read deps
    local urls=$(echo "$package" | jq -r .deps[])
    # clone
    cd $DIR"/deps"
    for url in ${urls[@]}; do
        git clone $url
    done
    echo -e "Cloning done."
}

symlinks() {
    echo -e "Creating symlinks..."
    cd $DIR
    # read symlinks
    local linkdirs=($(echo "$package" | jq -r '.symlinks | keys[]'))
    for linkdir in ${linkdirs[@]}; do
        echo -e "  > Creating symlinks in "$linkdir
        cd $DIR
        local targets=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | keys_unsorted[]'))
        local links=($(echo "$package" | jq --arg k "$linkdir" -r '.symlinks[$k] | .[]'))
        # create symlinks
        cd $DIR"/"$linkdir
        for (( i=0; i<${#targets[@]}; i++ )); do
            link=${links[$i]}
            target=${targets[$i]}
            if [[ -f $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            elif [[ -d $link ]]; then
                echo -e "    > "$target" -> "$linkdir$link" already exists."
            else
                ln -sf $target $link
                echo -e "    > "$target" -> "$linkdir$link" created."
            fi
        done
    done
}

help() {
    usage="project.sh [command]

    commands:
        start           (prod) Start application on nodejs
        deploy          (prod) Deploy application on server using rsync
        deploy-restart  (prod) Deploy application on server using rsync and restart systemd services
        lint            (dev) Run eslint
        test            (dev) Run tests
        build           (dev) Build application
        build-deps      (dev) Build dependencies list (done in build) -- needs Deno
        setup           (dev) Get depencencies (deps) and Create symlinks to dependencies (symlinks)
        help            Display help
        "
    echo "$usage"
}

# args
case $1 in
    lint)
        #eslint src --ext .js;;
        deno lint --ext=js src/frontend/feeds/
        deno lint --ext=js src/server/feeds/;;
    test)
        node ./tests/run.js;;
    build) 
        build;;
    setup)
        setup;;
    deploy)
        deploy;;
    deploy-restart)
        deploy "restart";;
    *|help)
        help;;
esac
