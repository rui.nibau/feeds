#!/bin/bash

set -e

DIR="$( cd "$( dirname "$0" )" && pwd )"
cd $DIR

run_node() {
    node ./src/feeds.js start
}

run_deno() {
    deno run --allow-net --allow-write --allow-read ./src/feeds.js start
}

# args
case $1 in
    node)
        run_node;;
    *|deno) 
        run_deno;;
esac
