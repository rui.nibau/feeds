
/**
 * 
 * @param {number} delay 
 * @returns {Promise}
 */
export const wait = delay => {
    return new Promise(resolve => setTimeout(() => resolve(), delay));
};

/**
 * @param {Function} fn Function to call after timout ms
 * @param {number} timeout Time in ms
 * @returns {Function} Debounced function
 */
export const debounce = (fn, timeout) => {
    let timer;
    return (...args) => {
        globalThis.clearTimeout(timer);
        // eslint-disable-next-line no-invalid-this
        timer = globalThis.setTimeout(() => fn.apply(this, args), timeout);
    };
};

/**
 * 
 * @param {string} action 
 * @param {string} text 
 * @param {string} title
 * @param {EventListener} [clickListener]
 * @returns {HTMLButtonElement}
 */
export const createActionButton = (action, text, title, clickListener) => {
    const button = globalThis.document.createElement('button');
    button.className = `action ${action}`;
    button.textContent = text || action;
    button.title = title;
    if (clickListener) {
        button.addEventListener('click', clickListener);
    }
    return button;
};

/**
 * @param {string} str 
 * @param {boolean} [clean = true]
 * @param {DOMParserSupportedType} [type='text/xml']
 * @returns {Document}
 */
export const parseXml = (str, clean = true, type = 'text/xml') => {
    const domParser = new globalThis.DOMParser();
    const doc = domParser.parseFromString(str, type);
    if (doc.documentElement.localName == 'parsererror') {
        throw new Error (`Parse error: ${doc.documentElement.textContent}`);
    }
    if (clean) {
        doc.querySelectorAll('frame,frameset,fencedframe,input,script,noscript,style,iframe,object,portal,embed, canvas')
            .forEach(element => element.remove());
        doc.querySelectorAll('*').forEach(element => {
            for (const attribute of element.attributes) {
                if (attribute.name.startsWith('on')) {
                    element.removeAttribute(attribute.name);
                }
            }
        });
    }
    return doc;
};
