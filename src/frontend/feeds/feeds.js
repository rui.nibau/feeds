import HTMLActions from '../rnb/lib/ui/actions/HtmlActions.js';
import notifications from '../rnb/lib/ui/notifications/notifications.js';
import { ACTION_BOOKMARK } from './bookmarks.js';

import { EVENT_FEEDS_UPDATE_START, EVENT_FEEDS_UPDATE_STOP, EVENT_FEED_ITEM_READ, EVENT_FEED_LOADED, EVENT_FEED_LOADED_ERROR } from './events.js';
import service from './service.js';
import { parseXml, wait } from './utils.js';

const domParser = new globalThis.DOMParser();

const WAIT_TIME = 66;

/**
 * @type {{[url:string]: FeedData}}
 */
let feedsMap = {};

let updating = false;

/**@type {import('../rnb/lib/ui/actions/HtmlActions.js').Action} */
let actionRead = null;

/**
 * 
 * @param {string} url 
 * @returns {number}
 */
const getUnreads = url => {
    if (feedsMap[url]) {
        return Object.values(feedsMap[url].articles).filter(a => a !== null).length;
    }
    return 0;
};

/**
 * @param {string} event 
 * @param {string} url 
 * @param {number} unreadsChange
 * @returns {import('./events.js').FeedEvent}
 */
export const createFeedEvent = (event, url, unreadsChange) => {
    return new CustomEvent(event, {detail: {url, unreadsChange}});
};


const feeds = {

    /**
     * 
     * @param {SerializedFeeds} feedsSerialized 
     * @param {string[]} urls Feed urls
     * @param {import('../rnb/lib/ui/actions/HtmlActions.js').Action} read
     */
    init(feedsSerialized, urls, read) {
        feedsMap = {};
        actionRead = read;
        if (!feedsSerialized) {
            return;
        }
        Object.entries(feedsSerialized || {}).forEach(([feedUrl, feed]) => {
            if (urls.includes(feedUrl)) {
                feedsMap[feedUrl] = { last: feed.last || 0, title: feed.title, articles: {}};
                Object.entries(feed.articles).forEach(([itemUrl, articleString]) => {
                    if (articleString !== null) {
                        const doc = domParser.parseFromString(articleString, 'text/html');
                        const article = doc.body.firstElementChild;
                        article.querySelectorAll('.item-title > a, rnb-actions button[data-name="read"]')
                            .forEach(el => el.addEventListener('click', read.action));
                        article.querySelector('rnb-actions input[data-name="bookmark"]')
                            ?.addEventListener('click', ACTION_BOOKMARK.action);
                        feedsMap[feedUrl].articles[itemUrl] = article;
                    } else {
                        feedsMap[feedUrl].articles[itemUrl] = null;
                    }
                });
                globalThis.dispatchEvent(createFeedEvent(EVENT_FEED_LOADED, feedUrl, getUnreads(feedUrl)));    
            }
        });
    },

    /**
     * Load specific feed
     * 
     * @param {string} url 
     * @param {0|1|2} [reloadOrClear] 0: no reload, 1: reload, 2: clear & reload
     * @returns {Promise<Element[]>} Articles of the feed
     */
    async load(url, reloadOrClear = 0) {
        if (!feedsMap[url] || reloadOrClear > 0) {
            console.log(`<${url}>: load`);
            const unreads = getUnreads(url);
            if (reloadOrClear === 2) {
                delete feedsMap[url];
            }
            try {
                await feeds.read(url);
                globalThis.dispatchEvent(createFeedEvent(EVENT_FEED_LOADED, url, getUnreads(url) - unreads));
            } catch(err) {
                console.error(`<${url}>: error`);
                console.error(err);
                globalThis.dispatchEvent(createFeedEvent(EVENT_FEED_LOADED_ERROR, url, unreads));
                return [];
            }
        }
        return Object.values(feedsMap[url].articles);
    },

    /**
     * Update feeds.
     *
     * @param {string[]} urls
     * @param {0|1|2} [reloadOrClear] 0: no reload, 1: reload, 2: clear & reload
     * @returns {Promise}
     */
    update(urls, reloadOrClear = 1) {
        if (updating) {
            return Promise.resolve();
        }
        updating = true;
        globalThis.dispatchEvent(new Event(EVENT_FEEDS_UPDATE_START));
        return Promise.all(urls.map(async (url, i) => {
            await wait(i * WAIT_TIME);
            return updating ? this.load(url, reloadOrClear) : Promise.resolve();
        })).finally(() => {
            updating = false;
            globalThis.dispatchEvent(new Event(EVENT_FEEDS_UPDATE_STOP));
        });
    },

    /**
     * @param {string} url 
     * @returns {Promise<boolean>}
     */
    async read(url) {
        const oldFeedData = feedsMap[url] || {last:0, title: '', articles: {}};

        let lastModified = 0;
        // const headers = await service.getFeedInfos(url);
        const {headers, body} = await service.getFeed(url);
        if (headers && (headers['last-modified'] || headers['date'])) {
            lastModified = (new Date(headers['last-modified'] || headers['date'])).getTime();
            if (lastModified <= oldFeedData.last) {
                console.log(`<${url}>: not updated since last visit (Header last-modified)`);
                return false;
            }
        }

        // const {body} = await service.getFeed(url);
        /**@type {Document} */
        let doc = null;
        try {
            doc = parseXml(body);
        } catch(err) {
            notifications.ko(err.message);
            return false;
        }

        const type = doc.documentElement.tagName.toLowerCase() === 'rss' ? 'rss' : 'atom';
        const title = readFeedTitle(type, doc);

        const feedData = {last: readLastUpdate(type, doc), title, articles: {}};
        
        if (feedData.last <= oldFeedData.last) {
            console.log(`<${url}>: not updated since last visit (feed last update)`);
            return false;
        }

        feedsMap[url] = feedData;

        // read items
        /**@type {(HTMLElement|Element)[]} */
        const articles = [];
        /**@type {string[]} */
        const urls = [];
        doc.querySelectorAll(feedTypes[type].item).forEach(item => {
            const itemUrl = readItemUrl(item);
            if (oldFeedData.articles[itemUrl] === undefined || oldFeedData.articles[itemUrl] !== null) {
                articles.push(createArticle(url, type, item));
            }
            urls.push(itemUrl);
        });
        const nulls = [];
        Object.entries(oldFeedData.articles).forEach(([itemUrl, article]) => {
            if (article !== null && !urls.includes(itemUrl)) {
                articles.push(article);
            } else if (article === null && urls.includes(itemUrl)) {
                nulls.push(itemUrl);
            }
        });

        articles.sort(feeds.sorter).forEach(article => {
            feedData.articles[article.dataset.url] = article;
        });
        nulls.forEach(url => feedData.articles[url] = null);

        if (lastModified > feedData.last) {
            feedData.last = lastModified;
        }

        return true;   
    },

    /**
     * Stop updating process if any
     */
    stopUpdate() {
        updating = false;
    },

    isUpdating() {
        return updating;
    },

    /**
     * 
     * @param {string} feedUrl 
     * @param {string} itemUrl 
     */
    markArticleAsRead(feedUrl, itemUrl) {
        if (feedUrl in feedsMap && itemUrl in feedsMap[feedUrl].articles) {
            feedsMap[feedUrl].articles[itemUrl] = null;
            globalThis.dispatchEvent(createFeedEvent(EVENT_FEED_ITEM_READ, feedUrl, -1));
        }
    },

    /**
     * Mark feeds as read
     * 
     * @param {string[]} urls Feed urls
     */
    markFeedsAsRead(urls) {
        urls.forEach(url => {
            if (url && feedsMap[url]) {
                Object.keys(feedsMap[url].articles).forEach(itemUrl => {
                    feedsMap[url].articles[itemUrl] = null;
                    globalThis.dispatchEvent(createFeedEvent(EVENT_FEED_ITEM_READ, url, -1));
                });
            }
        });
    },
    
    /**
     * Delete feeds
     *
     * @param {string[]} urls Urls 
     * @returns {boolean} if something has been deleted
     */
    delete(urls) {
        let changed = false;
        urls.forEach(url => {
            if (url in feedsMap) {
                changed = true;
                delete feedsMap[url];
            }
        });
        return changed;
    },

    /**
     * 
     * @param {string} url 
     * @returns {boolean}
     */
    has(url) {
        return url in feedsMap;
    },
    
    serialize() {
        const json = {};
        Object.entries(feedsMap).forEach(([feedUrl, feedData]) => {
            json[feedUrl] = {last: feedData.last, articles: {}};
            Object.entries(feedData.articles).forEach(([itemUrl, article]) => {
                json[feedUrl].articles[itemUrl] = article !== null 
                    ? new globalThis.XMLSerializer().serializeToString(article) 
                    : null;
            });
        });
        return JSON.stringify(json);
    },
    /**
     * @param {HTMLElement} a 
     * @param {HTMLElement} b 
     * @returns {number}
     */
    sorter: (a, b) => (b ? b.dataset.date || '' : '').localeCompare(a ? a.dataset.date : ''),
};

const feedTypes = {
    atom: {
        feedTitle: 'feed > title',
        last: ['updated'],
        item: 'entry',
        date: ['updated', 'published'],
        title: 'title',
        link: 'link',
        content: ['summary', 'content'],
        resource: 'link[rel="enclosure"]',
    },
    rss: {
        feedTitle: 'channel > title',
        last: ['channel > lastBuildDate', 'channel > pubDate'],
        item: 'item',
        date: ['pubDate'],
        title: 'title',
        link: 'link',
        content: ['description', 'content', 'content:encoded'],
        resource: 'enclosure',
    },
};

export default feeds;


/**
 * @param {'atom'|'rss'} type 
 * @param {Element} item 
 * @returns {number}
 */
const readItemDate = (type, item) => {
    for (const tag of feedTypes[type].date) {
        const element = item.querySelector(tag);
        if (element) {
            const date = new Date(element.textContent);
            return date.getTime();
        } 
    }
    return 0;
};

/**
 * @param {Element} item 
 * @returns {string}
 */
const readItemUrl = item => {
    const link = item.querySelector('link');
    if (link) {
        if (link.textContent) {
            return link.textContent;
        }
        return link.getAttribute('href');
    }
    return '';
};

/**
 * 
 * @param {'atom'|'rss'} type 
 * @param {Element} item 
 * @returns {string}
 */
const readItemResource = (type, item) => {
    const element = item.querySelector(feedTypes[type].resource);
    if (element) {
        return element.getAttribute(type === 'rss' ? 'url' : 'href');
    }
    return null;
};

/**
 * 
 * @param {string} type 
 * @param {Document} doc 
 * @returns {string}
 */
const readFeedTitle = (type, doc) => {
    const title = doc.querySelector(feedTypes[type].feedTitle);
    return title ? title.textContent : '';
};

/**
 * 
 * @param {string} type 
 * @param {Document} doc 
 * @returns {number}
 */
const readLastUpdate = (type, doc) => {
    for (const query of feedTypes[type].last) {
        const element = doc.querySelector(query);
        if (element) {
            const date = new Date(element.textContent);
            return date.getTime();
        }
    }
    return Date.now();
};

/**
 * @param {Element} item 
 * @param {string} query 
 * @returns {Element}
 */
const readElement = (item, query) => {
    if (query.includes(':')) {
        const els = item.getElementsByTagName(query);
        return els.length > 0 ? els[0] : null;
    }
    return item.querySelector(query);
};

const RE_IMG = /.(jpg|png|gif|webp)$/i;

/**
 * @param {string} url
 * @param {'rss'|'atom'} type 
 * @param {Element} item 
 * @returns {HTMLElement}
 */
const createArticle = (url, type, item) => {

    const itemUrl = readItemUrl(item);

    // title
    const a = globalThis.document.createElement('a');
    a.href = itemUrl;
    a.textContent = item.querySelector('title')?.textContent;
    a.title = a.href;
    a.target = '_blank';
    a.addEventListener('click', actionRead.action);
    const h = globalThis.document.createElement('h2');
    h.className = 'item-title';
    h.append(a);

    // data
    const data = globalThis.document.createElement('div');
    data.className = 'item-data';

    const date = readItemDate(type, item);
    const time = globalThis.document.createElement('time');
    time.textContent = (new Date(date)).toLocaleDateString();
    
    const feedName = globalThis.document.createElement('cite');
    feedName.textContent = feedsMap[url].title;

    data.append(time, ' — ', feedName);

    // content

    const content = globalThis.document.createElement('div');
    content.className = 'item-content';
    feedTypes[type].content.forEach(tag => {
        const el = readElement(item, tag);
        if (el) {
            if (el.getAttribute('type') === 'html') {
                content.append(getElementFromContent(el.textContent));
            } else if (el.childElementCount > 0) {
                for (const child of el.children) {
                    content.append(globalThis.document.importNode(child, true));
                }
            } else if (el.childNodes.length > 0) {
                for (const childNode of el.childNodes) {
                    // eslint-disable-next-line no-undef
                    if (childNode instanceof CDATASection) {
                        content.append(getElementFromContent(childNode.data));
                    } else {
                        content.append(getElementFromContent(childNode.textContent));
                    }
                }
            }
        }
    });

    // resources
    const res = readItemResource(type, item);
    if (res) {
        let reContent = null;
        if (RE_IMG.test(res)) {
            reContent = globalThis.document.createElement('img');
            reContent.loading = 'lazy';
            reContent.src = res;
        } else {
            reContent = globalThis.document.createElement('a');
            reContent.target = '_blank';
            reContent.textContent = '▶';
            reContent.title = res;
            reContent.href = res;    
        }
        const resElement = globalThis.document.createElement('div');
        resElement.className = 'item-resource';
        resElement.append(reContent);
        content.append(resElement);
    }

    // targets
    content.querySelectorAll('a').forEach(a => a.target = '_blank');

    const article = globalThis.document.createElement('article');
    article.dataset.feedName = feedsMap[url].title;
    article.dataset.feed = url;
    article.dataset.url = itemUrl;
    article.dataset.date = String(date);
    
    article.append(new HTMLActions({actions: [ACTION_BOOKMARK, actionRead]}), data, h, content);

    return article;
};

const RE_STYLE = /style="(?:[^"]+)"/gi;

/**
 * @param {string} content 
 * @returns {string}
 */
const getContent = content => content.replaceAll(RE_STYLE, '').replaceAll('<img ', '<img loading="lazy" ');

/**
 * @param {string} content 
 * @returns {Element}
 */
const getElementFromContent = content => {
    return domParser.parseFromString(`<div>${getContent(content)}</div>`, 'text/html').body.firstElementChild;
};
