import __ from '../rnb/lib/data/i18n/i18n.js';
import HTMLActions from '../rnb/lib/ui/actions/HtmlActions.js';
import service from './service.js';

/**
 * @type {HTMLElement[]}
 */
let bookmarksList = [];


const bookmarks = {

    load() {
        return service.getBookmarks().then(doc => {
            // Attach listeners
            doc.querySelectorAll('rnb-actions button[data-name="remove"]').forEach(button => {
                button.addEventListener('click', ACTION_REMOVE.action);
            });
            bookmarksList = Array.from(doc.documentElement.lastElementChild.children);
        });
    },

    save() {
        service.setBookmarks(bookmarksList);
    },
    
    getAll() {
        return bookmarksList;
    },

    /**
     * 
     * @param {HTMLElement} article 
     * @returns {boolean}
     */
    add(article) {
        const url = article.dataset.url;
        if (!bookmarksList.find(i => i.dataset.url === url)) {
            const item = /**@type {HTMLElement}*/(article.cloneNode(true));
            // edit actions
            const actions = /**@type {HTMLActions}*/(item.querySelector('rnb-actions'));
            actions.remove();
            item.prepend(new HTMLActions({actions: [ACTION_REMOVE]}));
            bookmarksList.unshift(item);
            bookmarks.save();
            return true;
        }
        return false;
    },

    /**
     * 
     * @param {string | HTMLElement} articleOrUrl 
     * @returns {boolean}
     */
    remove(articleOrUrl) {
        const url = typeof articleOrUrl === 'string'
            ? articleOrUrl
            : articleOrUrl.dataset.url;
        const index = bookmarksList.findIndex(i => i.dataset.url === url);
        if (index > -1) {
            const removed = bookmarksList.splice(index, 1);
            removed[0].remove();
            bookmarks.save();
            return true;
        }
        return false;
    },
};

export default bookmarks;

export const ICON_BOOKMARK = '★';

/**
 * @type {import('../rnb/lib/ui/actions/HtmlActions.js').Action}
 */
const ACTION_REMOVE = {
    name: 'remove',
    label: '✖',
    title: __('removeBookmarkDescription'),
    action(e) {
        const button = /**@type {HTMLElement}*/(e.target);
        const article = button.closest('article');
        bookmarks.remove(article);
    },
};

/**
 * @type {import('../rnb/lib/ui/actions/HtmlActions.js').Action}
 */
export const ACTION_BOOKMARK = {
    name: 'bookmark',
    label: ICON_BOOKMARK,
    title: __('toggleBookmarkDescription'),
    type: 'boolean',
    action(e) {
        const checkbox = /**@type {HTMLInputElement}*/(e.target);
        const article = checkbox.closest('article');
        if (article) {
            if (checkbox.checked) {
                bookmarks.add(article);
            } else {
                bookmarks.remove(article);
            }
        }
    },
};
