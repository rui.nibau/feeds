
export default class HTMLFeedsProgress extends HTMLElement {

    /**@private */
    updatingProgress = (() => {
        const updatingProgress = globalThis.document.createElement('progress');
        updatingProgress.max = 0;
        updatingProgress.value = 0;
        updatingProgress.id = 'progress-update';
        return updatingProgress;
    })();

    /**@private */
    unreadBadge = (() => {
        const unreadBadge = globalThis.document.createElement('span');
        unreadBadge.className = 'progress-badge';
        return unreadBadge;
    })();

    constructor() {
        super();
        this.id = 'rnb-feeds-toolbar';
        this.append(this.updatingProgress, this.unreadBadge);
    }

    get max() {
        return this.updatingProgress.max;
    }

    /**
     * @param {number} value
     */
    set max(value) {
        this.updatingProgress.max = value;
    }

    get value() {
        return this.updatingProgress.value;
    }

    /**
     * @param {number} value
     */
    set value(value) {
        const text = `${value} / ${this.updatingProgress.max}`;
        this.updatingProgress.value = value;
        this.updatingProgress.title = text;
        this.updatingProgress.textContent = text;
    }

    get unread() {
        return this.unreadBadge.textContent ? Number(this.unreadBadge.textContent) : 0;
    }

    /**
     * @param {number} value
     */
    set unread(value) {
        if (value < 0) {
            value = 0;
        }
        this.unreadBadge.textContent = `${value}`;
    }
}

globalThis.customElements.define('rnb-feeds-progress', HTMLFeedsProgress);
