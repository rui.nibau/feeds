import HTMLActions from '../rnb/lib/ui/actions/HtmlActions.js';

export default class HTMLFeedView extends HTMLElement {

    titleElement = globalThis.document.createElement('h1');

    contentElement = globalThis.document.createElement('div');

    /**
     * @param {import('../rnb/lib/ui/actions/HtmlActions.js').Action[]} actions 
     */
    constructor(actions = []) {
        super();
        this.id = 'feeds-view';
        const div = globalThis.document.createElement('div');
        this.actionsElement = new HTMLActions({actions});
        div.append(this.titleElement, this.actionsElement);
        this.append(div, this.contentElement);
    }

    clear() {
        this.titleElement.textContent = '';
        this.contentElement.innerHTML = '';
        this.actionsElement.disabled = true;
    }

    /**
     * 
     * @param {string} title 
     * @param {Element[]} items 
     */
    update(title, items) {
        this.titleElement.textContent = title;
        this.contentElement.innerHTML = '';
        this.contentElement.append(...items.filter(item => item !== null));
        this.actionsElement.disabled = !items || items.length === 0;
    }

    get feedTitle() {
        return this.titleElement.textContent;
    }

    /**
     * Mark all current articles as read
     */
    readAll() {
        this.contentElement.querySelectorAll('rnb-actions button[data-name="read"]').forEach(btn => btn.click());
    }
}

globalThis.customElements.define('rnb-feeds-view', HTMLFeedView);
