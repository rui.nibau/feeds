import HTMLActions from '../rnb/lib/ui/actions/HtmlActions.js';
import HTMLOpml, { getOutline } from '../rnb/lib/ui/opml/HTMLOpml.js';
import notifications from '../rnb/lib/ui/notifications/notifications.js';
import { saveFile } from '../rnb/lib/ui/save/saves.js';

import feeds from './feeds.js';
import HTMLFeedView from './HTMLFeedView.js';
import HTMLFeedsProgress from './HTMLFeedsProgress.js';
import service from './service.js';
import { EVENT_FEEDS_UPDATE_STOP, EVENT_FEED_ITEM_READ, EVENT_FEED_LOADED, EVENT_FEED_LOADED_ERROR } from './events.js';
import { debounce } from './utils.js';
import HTMLDialog from '../rnb/lib/ui/dialogs/HTMLDialog.js';
import HTMLLayout from '../rnb/lib/ui/layout/HTMLLayout.js';
import __, { i18n } from '../rnb/lib/data/i18n/i18n.js';
import bookmarks, { ICON_BOOKMARK } from './bookmarks.js';

const ATTR_XML_URL = 'xmlUrl';

/**
 * @typedef {import('../rnb/lib/ui/opml/HTMLOpml.js').OutlineEvent} OutlineEvent
 * @typedef {import('../rnb/lib/ui/opml/HTMLOpml.js').HTMLOutline} HTMLOutline
 * @typedef {import('../rnb/lib/ui/actions/HtmlActions.js').Action} Action
 */

(async() => {

    await i18n.upload();

    // Arrêt serveur quand lancer en mode app
    const params = new globalThis.URLSearchParams(globalThis.location.search);
    if (params.get('mode') === 'app') {
        globalThis.addEventListener('beforeunload', () => {
            globalThis.navigator.sendBeacon('/stop');
        }); 
    }
    
    /**
     * Add a feed
     *
     * @type {Action}
     */
    const ACTION_ADD = {
        name: 'add', 
        label: '✚', 
        title: __('addDescription'),
        action(e) {
            opmlElement.createOutline(getOutline(/**@type {HTMLElement}*/(e.target)));
        },
        visible(actions) {
            const outline = getOutline(actions);
            if (outline) {
                return !outline.getAttribute(ATTR_XML_URL);
            }
            return true;
        }};

    /**
     * Download feeds as opml document
     *
     * @type {Action}
     */
    const ACTION_DOWNLOAD = {
        name: 'download', 
        label: '↧', 
        title: __('downloadDescription'), 
        action: () => {
            saveFile(opmlElement.serialize(), 'application/xml', 'feeds.opml');
        },
    };

    /**
     * Force the reload of a feed
     * 
     * @type {Action}
     */
    const ACTION_RELOAD_FORCE = {
        name: 'force', 
        label: '🗘',
        title: __('forceReloadDescription'),
        action(e) {
            reloadFeed(getOutline(/**@type {HTMLElement}*/(e.target)) || opmlElement.selected, true);
        },
    };
    
    /**
     * Reload feed
     * 
     * @type {Action}
     */
    const ACTION_RELOAD = {
        name: 'reload',
        label: '⟲',
        title: __('reloadDescription'),
        action(e) {
            reloadFeed(getOutline(/**@type {HTMLElement}*/(e.target)) || opmlElement.selected);
        },
    };

    /**
     * Mark a feed, a article or a list of articles as read
     * 
     * @type {Action}
     */
    const ACTION_READ = {
        name: 'read',
        label: '🗸',
        title: __('markReadDescription'),
        action: e => {
            const outline = getOutline(/**@type {HTMLElement}*/(e.target));
            if (outline) { // from outline
                if (outline === opmlElement.selected) {
                    viewElement.readAll();
                } else {
                    feeds.markFeedsAsRead([outline.getAttribute(ATTR_XML_URL), ...outline.getAttributes(ATTR_XML_URL)]);
                }
            } else { // from view or from article
                const article = /**@type {HTMLElement}*/(e.target).closest('article');
                if (article) {
                    feeds.markArticleAsRead(article.dataset.feed, article.dataset.url);
                    article.remove();
                } else {
                    viewElement.readAll();
                }
            }
            saveFeeds();
        },
    };

    /**
     * Show bookmarks
     *
     * @type {Action}
     */
    const ACTION_BOOKMARKS = {
        name: 'bookmarks',
        label: ICON_BOOKMARK,
        title: __('Bookmarks'),
        disable() {
            return viewElement.feedTitle === __('Bookmarks');
        },
        action: e => {
            const button = /**@type {HTMLElement}*/(e.target);
            button.classList.add('selected');
            opmlElement.select(null);
            viewElement.update(__('Bookmarks'), bookmarks.getAll());
            actionsElement.updateDisable();
        },
    };

    /**
     * @type {Action}
     */
    const ACTION_UPDATE_ALL =  {
        name: 'update', 
        label: '⟲', 
        title: __('updateFeedsDescription'), 
        action: () => {
            updateStarted(updating = !updating);
        },
    };

    /**
     * @type {Action}
     */
    const ACTION_LOGS = {
        name: 'logs',
        label: '🗐',
        title: __('viewLogsDescription'),
        action() {
            service.getLogs().then(logs => {
                const dialog = new HTMLDialog({
                    title: __('clearLogTitle'),
                    content: logs,
                    actions: [{
                        type: 'button',
                        name: 'clear',
                        label: __('clearLabel'),
                        title: __('clearLogDescription'),
                        action() {
                            service.clearLogs().then(response => {
                                dialog.content.textContent = '';
                                notifications.ok(response.message);
                            }).catch(err => {
                                notifications.ko(`${__('clearLogErrorMessage')}: ${err}`);
                            });
                        },
                    }, {
                        type: 'submit',
                        name: 'close',
                        label: __('closeLabel'),
                    }],
                });
                dialog.classList.add('logs');
                dialog.showModal();
            }).catch(err => {
                notifications.ko(`${__('readLogsErrorMessage')}: ${err}`);
            });
        },
    };


    let updating = false;

    let totalFeedsLoaded = 0;
    
    const viewElement = new HTMLFeedView([ACTION_READ]);

    const actionsElement = new HTMLActions({actions: [ACTION_UPDATE_ALL, ACTION_ADD, ACTION_BOOKMARKS, ACTION_DOWNLOAD,
        ACTION_LOGS]});

    const progressElement = new HTMLFeedsProgress();

    const opmlElement = new HTMLOpml(await service.getOpml(), [ATTR_XML_URL], [
        HTMLOpml.ACTION_DELETE,
        HTMLOpml.ACTION_EDIT, 
        ACTION_ADD,
        ACTION_RELOAD_FORCE,
        ACTION_RELOAD,
        ACTION_READ,
    ]);

    opmlElement.addEventListener(HTMLOpml.EVENT_OUTLINE_SELECTED, async () => {
        const url = opmlElement.selected.getAttribute(ATTR_XML_URL);
        if (url) {
            viewElement.update(opmlElement.selected.text, await feeds.load(url));
        } else {
            const articles = [];
            Promise.all(opmlElement.selected.getAttributes(ATTR_XML_URL).map(async url => {
                articles.push(...await feeds.load(url));
            })).catch(err => {
                console.log(err);
            }).then(() => {
                articles.sort(feeds.sorter);
                viewElement.update(opmlElement.selected.text, articles);
            });
        }
        actionsElement.updateDisable();
    });

    // Suppression d'un outline
    opmlElement.addEventListener(HTMLOpml.EVENT_OUTLINE_REMOVE, (/**@type {OutlineEvent}*/e) => {
        const outline = e.detail.outline;
        if (outline.text === viewElement.feedTitle) {
            viewElement.clear();
        }
        const oldValue = outline.value;
        outline.value = 0;
        progressElement.unread -= oldValue;
        feeds.delete([outline.getAttribute(ATTR_XML_URL), ...outline.getAttributes(ATTR_XML_URL)]);
        saveFeeds();
        saveOpml();
    });

    // outline create
    opmlElement.addEventListener(HTMLOpml.EVENT_OUTLINE_CREATE, (/**@type {OutlineEvent}*/e) => {
        const data = e.detail.data;
        if (data.attributes && feeds.has(data.attributes[ATTR_XML_URL])) {
            e.preventDefault();
            e.stopPropagation();
            e.stopImmediatePropagation();
            notifications.ko(__('entryAlreadyExistsMessage', {
                url: data.attributes[ATTR_XML_URL],
            }));
        } else {
            saveOpml();
            notifications.ok(__('entryCreatedMessage', {
                text: data.text,
                url: data.attributes[ATTR_XML_URL]||'-',
            }));
        }
    });

    // outline update
    opmlElement.addEventListener(HTMLOpml.EVENT_OUTLINE_UPDATE, (/**@type {OutlineEvent}*/e) => {
        const prevUrl = e.detail.outline.getAttribute(ATTR_XML_URL);
        if (e.detail.data && e.detail.data.attributes && e.detail.data.attributes[ATTR_XML_URL] &&
                e.detail.data.attributes[ATTR_XML_URL] !== prevUrl) {
            feeds.delete([prevUrl]);
            saveFeeds();
        }
        saveOpml();
    });
    
    // outline move
    opmlElement.addEventListener(HTMLOpml.EVENT_OUTLINE_MOVE, (/**@type {OutlineEvent}*/e) => {
        // If newParent is feed, take parentElement
        if (e.detail.newParent.hasAttribute(ATTR_XML_URL)) {
            e.detail.newParent = /**@type {HTMLOutline}*/(e.detail.newParent.parentElement);
        }
        saveOpml();
    });

    const onFeedUpdateLoadOrError = (/**@type {import('./events.js').FeedEvent}*/e) => {
        if (feeds.isUpdating() && progressElement.max > 0) {
            totalFeedsLoaded += 1;
            progressElement.value = totalFeedsLoaded;
        }
        const outline = opmlElement.getOutline('xmlUrl', e.detail.url);
        if (outline) {
            outline.value += e.detail.unreadsChange;
            outline.dataset.state = e.type === EVENT_FEED_LOADED_ERROR ? 'ko' : 'ok';
        }
        progressElement.unread += e.detail.unreadsChange;

        if (e.detail.unreadsChange > 0) {
            saveFeeds();
        }
    };

    globalThis.addEventListener(EVENT_FEED_ITEM_READ, onFeedUpdateLoadOrError);
    globalThis.addEventListener(EVENT_FEED_LOADED, onFeedUpdateLoadOrError);
    globalThis.addEventListener(EVENT_FEED_LOADED_ERROR, onFeedUpdateLoadOrError);
    globalThis.addEventListener(EVENT_FEEDS_UPDATE_STOP, () => {
        globalThis.setTimeout(() => {
            updateStarted(false);
        }, 500);
    });

    /**
     * Reload d'un outline
     * 
     * @param {HTMLOutline} outline 
     * @param {boolean} [force] 
     */
    const reloadFeed = async (outline, force) => {
        if (!outline) {
            console.error('reloadFeed with no outline');
            return;
        }
        const url = outline.getAttribute(ATTR_XML_URL);
        if (url) {
            const items = await feeds.load(url, force ? 2 : 1);
            if (opmlElement.selected && opmlElement.selected.getAttribute(ATTR_XML_URL) === url) {
                viewElement.update(outline.text, items);
            }
        } else {
            updateFeeds(outline.getAttributes(ATTR_XML_URL), force ? 2 : 1);
        }
    };

    /**
     * 
     * @param {string[]} urls 
     * @param {0|1|2} [reloadOrClear]
     */
    const updateFeeds = (urls, reloadOrClear) => {
        progressElement.value = 0;
        progressElement.max = urls.length;
        totalFeedsLoaded = 0;
        const start = Date.now();
        feeds.update(urls, reloadOrClear).then(() => {
            let time = (Date.now() - start) / 1000;
            let unit = 's';
            if (time > 60) {
                time = time / 60;
                unit = 'min';
            }
            notifications.ok(__('updateTimeMessage', {
                count: String(urls.length),
                unread: String(progressElement.unread),
                time: time.toFixed(2),
                unit,
            }), true);
        });
    };

    /**
     * @param {boolean} isUpdating 
     */
    const updateStarted = isUpdating => {
        const updateButton = actionsElement.getActionElement('update');
        updating = isUpdating;
        if (isUpdating) {
            updateButton.textContent = '⏹';
            updateButton.title = __('stopUpdateDescription');
            updateFeeds(opmlElement.getAttributes(ATTR_XML_URL));
        } else {
            updateButton.textContent = '⟲';
            updateButton.title = __('updateFeedsDescription');
            feeds.stopUpdate();
            totalFeedsLoaded = 0;
            progressElement.value = 0;
            progressElement.max = 0;
        }
    };

    let needsSaving = false;

    globalThis.addEventListener('beforeunload', e => {
        if (needsSaving) {
            e.preventDefault();
        }
    });

    const saveOpml = () => {
        needsSaving = true;
        saveOpmlDebounce();
    };
    const saveOpmlDebounce = debounce(() => {
        service.setOpml(opmlElement.serialize());
        needsSaving = false;
    }, 1500);

    const saveFeeds = () => {
        needsSaving = true;
        saveFeedsDebounce();
    };
    const saveFeedsDebounce = debounce(() => {
        service.setFeeds(feeds.serialize());
        needsSaving = false;
    }, 1500);

    feeds.init(await service.getFeeds(), opmlElement.getAttributes(ATTR_XML_URL), ACTION_READ);

    await bookmarks.load();

    const layout = new HTMLLayout({
        left: (globalThis.matchMedia('(hover: hover) and (pointer: fine)').matches &&
            globalThis.innerWidth > 780) ? 'fix' : 'over', 
    });
    layout.main.append(viewElement);
    layout.left.append(actionsElement, progressElement, opmlElement);
    globalThis.document.body.append(layout);
})();
