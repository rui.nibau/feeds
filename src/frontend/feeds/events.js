
export const EVENT_FEEDS_UPDATE_START = 'feeds-update-start';
export const EVENT_FEEDS_UPDATE_STOP = 'feeds-update-stop';

export const EVENT_OPML_EXPORT = 'feeds-opml-export';

export const EVENT_FEEDS_ADD = 'feeds-feeds-add';

export const EVENT_FEED_CREATE = 'feeds-feed-create';
export const EVENT_FEED_ITEM_READ = 'feeds-feed-item-read';
export const EVENT_FEED_READ = 'feeds-feed-read';
export const EVENT_FEED_LOADED = 'feeds-feed-loaded';
export const EVENT_FEED_LOADED_ERROR = 'feeds-feed-loded-error';



/**
 * @typedef FeedEventDetail
 * @property {string} url
 * @property {number} unreadsChange Unread article for this feed
 * @typedef {CustomEvent<FeedEventDetail>} FeedEvent
 */
