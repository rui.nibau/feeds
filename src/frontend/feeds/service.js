import notifications from '../rnb/lib/ui/notifications/notifications.js';
import { parseXml } from './utils.js';

class RequestError extends Error {
    /**
     * @param {RnbServerResponseBody} body 
     */
    constructor(body) {
        super();
        this.response = body;
    }
}

/**
 * Is a GET like method ?
 *
 * @param {string} method 
 * @returns {boolean}
 */
const isGetMethod = method => !method || method === 'GET' || method === 'HEAD';

/**
 * @param {string} url 
 * @param {'text'|'json'|'xml'|'html'} [format='text']
 * @param {RequestInit} [options = {method: 'GET'}]
 * @param {{notifyOk: boolean, notifyKo: boolean}} [cfg]
 * @returns {Promise<string|object|Document|RnbServerResponseBody>}.
 * @throws {RequestError}
 */
const request = (url, format = 'text', options = {method: 'GET'}, cfg = {notifyOk: true, notifyKo: true}) => {
    return globalThis.fetch(url, options)
        .then(async res => {
            if (res.ok) {
                if (format === 'json') {
                    const json = await res.json();
                    if (!isGetMethod(options.method) && 'message' in json && 'ok' in json) {
                        // Une réponse du serveur qui peut être notifié
                        if (json.ok) {
                            if (!cfg || cfg.notifyOk) {
                                notifications.ok(json.message, false);
                            }
                        } else if (!cfg || cfg.notifyKo) {
                            notifications.ko(json.message, false);
                        }
                    }
                    return json;
                }
                if (format === 'xml' || format === 'html') {
                    return res.text().then(text => parseXml(text, false, `text/${format}`));
                }
                return res.text();
            }
            // Error: server always return a json object
            const body = /**@type {RnbServerResponseBody}*/(await res.json());
            if (!cfg || cfg.notifyKo) {
                notifications.ko(`${options.method} ${url}: ${body.message}`);
            }
            throw new RequestError(body);
        })
        .catch(err => {
            console.error(err);
            if (err instanceof RequestError) {
                if (!cfg || cfg.notifyKo) {
                    notifications.ko(err.response.message);
                }
                throw err;
            } else {
                if (!cfg || cfg.notifyKo) {
                    notifications.ko(err);
                }
                throw new RequestError({ok: false, message: err.toString(), code: 500});    
            }
        });
};

export default {
    
    /**
     * @returns {Promise<string>} Logs or null on error
     * @throws {RequestError}
     */
    getLogs() {
        return request('/logs');
    },
    
    /**
     * @returns {Promise<RnbServerResponseBody>}
     * @throws {RequestError}
     */
    clearLogs() {
        return request('/logs', 'json', {method:'DELETE'});
    },

    /**
     * @returns {Promise<Document>} Document requested or null or error
     * @throws {RequestError}
     */
    getOpml() {
        return request('/opml', 'xml');
    },

    /**
     * @param {string} opmlString 
     * @returns {Promise<RnbServerResponseBody>}
     * @throws {RequestError}
     */
    setOpml(opmlString) {
        return request('/opml', 'json', { method: 'POST', body: opmlString });
    },

    /**
     * @param {string} url 
     * @returns {Promise<{headers:{[name:string]: string}, body:string}>} Feed requested or null on error 
     * @throws {RequestError}
     */
    getFeed(url) {
        return request(`/feed?url=${url}`, 'json');
    },

    /**
     * @param {string} url 
     * @returns {Promise<object>}
     * @throws {RequestError}
     */
    getFeedInfos(url) {
        return request(`/feed-infos?url=${url}`, 'json');
    },

    /**
     * @returns {Promise<SerializedFeeds>} SerializedFeed or null on error
     * @throws {RequestError}
     */
    getFeeds() {
        return request('/feeds', 'json');
    },

    /**
     * @param {string} feeds 
     * @returns {Promise<RnbServerResponseBody>}
     * @throws {RequestError}
     */
    setFeeds(feeds) {
        return request('/feeds', 'json', {method: 'POST', body: feeds}, 
            {notifyKo: true, notifyOk: false});
    },

    /**
     * 
     * @returns {Promise<Document>}
     */
    getBookmarks() {
        return request('/bookmarks', 'html');
    },

    /**
     * 
     * @param {HTMLElement[]} bookmarks 
     * @returns {Promise<RnbServerResponseBody>}
     * @throws {RequestError}
     */
    setBookmarks(bookmarks) {
        const serializer = new globalThis.XMLSerializer();
        return request('/bookmarks', 'json', {
            method: 'POST',
            body: bookmarks.map(article => serializer.serializeToString(article)).join(''),
        });
    },
};
