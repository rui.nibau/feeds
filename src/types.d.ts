// import { LoggerOptions } from './server/rnb/lib/logs/Logger'

type SerializedFeeds = {
    [url: string]: {
        last: number,
        title: string,
        articles: {
            [url: string]: (null|string)
        }
    }
}
type FeedData = {
    last: number,
    title: string,
    articles: {
        [url: string]: (HTMLElement|Element)
    }
}

/**
 * Application configuration that must be stored in data/config.json file
 */
type FeedsConfig = {
    server: {
        port?: number
        logs?: LoggerOptions
    }
    ssl?: {
        key: string
        cert: string
    }
    deploy?: {
        /**
         * Host for deployement
         */
        host?: string
        /**
         * Path on host where to deploy
         */
        path?: string
    }
}
