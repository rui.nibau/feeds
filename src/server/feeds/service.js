import FileLogger from '../rnb/lib/logs/FileLogger.js';
import Loggers from '../rnb/lib/logs/Loggers.js';
import fsys from '../rnb/lib/runtime/fsys.js';
import rsys from '../rnb/lib/runtime/rsys.js';

const BASE_PATH = fsys.pathFromMeta(import.meta, './../../../');
const WWW_PATH = `${BASE_PATH}src/frontend`;
export const DATA_PATH = `${BASE_PATH}data`;

const headers = {
    'Accept': 'application/xml, application/rss+xml, application/atom+xml, application/rdf+xml;q=0.8, text/xml;q=0.4', 
    'Accept-Encoding': 'gzip,inflate,br',
};

const OPML_EMPTY = `<opml><head><title>tree</title><dateCreated>${new Date().toString()}</dateCreated></head><body></body></opml>`;

const service = {
    /**
     * Create logger for application
     * 
     * @param {import('../rnb/lib/logs/Logger.js').LoggerOptions} options 
     * @returns {import('../rnb/lib/logs/Logger.js').default}
     */
    createLogger(options) {
        Loggers.set('feeds', new FileLogger(DATA_PATH, 'feeds', options));
        return Loggers.get('feeds');
    },

    /**
     * Returns application's logger
     * 
     * @returns {import('../rnb/lib/logs/Logger.js').default}
     */
    getLogger() {
        return Loggers.get('feeds');
    },
    
    /**
     * @param {string} relpath 
     * @returns {string}
     */
    getDataPath(relpath) {
        return fsys.getFilePath(DATA_PATH, relpath);
    },

    /**
     * @param {string} relpath 
     * @returns {string}
     */
    getClientPath(relpath) {
        return fsys.getFilePath(WWW_PATH, relpath === '/' ? '/index.html' : relpath);
    },

    /**
     * 
     * @param {boolean} [asJson] 
     * @returns {string|FeedsConfig}
     */
    getConfig(asJson = false) {
        return fsys.readJsonFile(fsys.getPath(DATA_PATH, 'config.json', false), asJson);
    },

    /**
     * 
     * @param {string} config 
     */
    setConfig(config) {
        const respath = fsys.getPath(DATA_PATH, 'config.json', false);
        fsys.writeTextFile(respath, config);
    },

    /**
     * Get opml as a string or as a Document (only on Deno)
     *
     * @param {boolean} [asXml] 
     * @returns {string | Document}
     */
    getOpml(asXml = false) {
        let content = '';
        const respath = fsys.getPath(DATA_PATH, 'feeds.opml', true);
        if (respath) {
            content = fsys.readTextFile(respath);
        } else {
            content = OPML_EMPTY;
        }
        if (asXml && rsys.IS_DENO) {
            const parser = new globalThis.DOMParser();
            try {
                return parser.parseFromString(content, 'application/xml');
            } catch(err) {
                console.error(err);
                return parser.parseFromString(OPML_EMPTY, 'application/xml');
            }
        }
        return content;
    },

    /**
     * Save opml
     *
     * @param {string} opml 
     */
    setOpml(opml) {
        const respath = fsys.getPath(DATA_PATH, 'feeds.opml', false);
        fsys.saveAndBackup(respath, opml);
    },

    /**
     * Update an url in the opml. usefull when feed returns a 301.
     *
     * @param {string} oldUrl 
     * @param {string} newUrl 
     */
    updateOpmlUrl(oldUrl, newUrl) {
        const respath = fsys.getPath(DATA_PATH, 'feeds.opml', true);
        if (respath) {
            fsys.writeTextFile(respath, fsys.readTextFile(respath).replace(oldUrl, newUrl));
        }
    },

    /**
     * Returns cached feeds content
     *
     * @returns {string}
     */
    getFeeds() {
        return fsys.readJsonFile(fsys.getPath(DATA_PATH, 'feeds.json', false));
    },

    /**
     * Update cache feeds content
     *
     * @param {string} feedsString 
     */
    setFeeds(feedsString) {
        const respath = fsys.getPath(DATA_PATH, 'feeds.json', false);
        fsys.writeTextFile(respath, feedsString);
    },

    /**
     * 
     * @returns {string}
     */
    getBookmarks() {
        return fsys.readTextFile(fsys.getPath(DATA_PATH, 'bookmarks.html', false));
    },

    /**
     * 
     * @param {string} bookmarks 
     */
    setBookmarks(bookmarks) {
        const respath = fsys.getPath(DATA_PATH, 'bookmarks.html', false);
        fsys.saveAndBackup(respath, bookmarks);
    },

    /**
     * request feed content
     *
     * @param {'GET'|'HEAD'} method
     * @param {string} url 
     * @returns {Promise<{body?:string, headers?:{[name:string]: string}|{}}>}
     */
    requestFeed(method, url) {
        const logger = service.getLogger();
        const time = Date.now();
        return globalThis.fetch(url, {
            method, 
            headers, 
            signal: AbortSignal.timeout(15000),
            cache: 'no-store',
            credentials: 'include',
        }).then(async response => {
            if (response.ok) {
                const headers = {};
                response.headers.forEach((value, name) => headers[name] = value);
                const body = method === 'GET' ? await response.text() : null;
                logger.info(`Feed: ${method} ${url} in ${(Date.now() - time)/1000} s`);
                return {headers, body};
            }
            if ((response.status === 301 || response.status === 302) && response.headers.has('location')) {
                const u = new URL(url);
                const loc = response.headers.get('location');
                const newUrl = loc.startsWith('/') ? `${u.protocol}//${u.host}/${loc}` : loc;
                logger.info(`Feed change: from <${url}> to <${newUrl}>`);
                service.updateOpmlUrl(url, newUrl);
                return service.requestFeed(method, newUrl);
            }
            const error = new Error(method === 'GET' ? (await response.text()) : response.statusText);
            error.name = 'FeedError';
            throw error;
        }).catch(error => {
            logger.error(`Feed: ${method} ${url} - ${error.toString()}`);
            throw error;
        });
    },
};

export default service;
