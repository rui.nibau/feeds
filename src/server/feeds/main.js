import Server from '../rnb/lib/http/Server.js';
import { sendJson, sendResource, sendResponse } from '../rnb/lib/http/responses.js';
import fsys from '../rnb/lib/runtime/fsys.js';

import service from './service.js';

/**
 * @param {'GET'|'HEAD'} method 
 * @param {Request | RnbIncomingMessage} req 
 * @param {import('node:http').ServerResponse} res 
 * @returns {Promise<Response | import('node:http').ServerResponse>}
 */
const doFeed = async (method, req, res) => {
    const url = new globalThis.URL(req.url);
    const urlFeed = url.searchParams.get('url');
    if (urlFeed) {
        try {
            return sendJson(res, await service.requestFeed(method, urlFeed));
        } catch(error) {
            return sendJson(res, {ok: false, message: error.message}, error.name === 'FeedError' ? 400 : 500);
        }
    }
    return sendJson(res, {ok: false, message: 'Bas url feed in url'}, 500);
};

/**
 * @type {import('../rnb/lib/logs/Logger.js').default}
 */
let logger = null;

export default {

    start() {
        
        const config = /**@type {FeedsConfig}*/(service.getConfig(true));

        logger = service.createLogger(config.server && config.server.logs ? config.server.logs : {});

        logger.info(`config: ${JSON.stringify(config)}`);

        let key = null, cert = null;
        if (config.ssl && config.ssl.cert && config.ssl.cert) {
            key =  fsys.readTextFile(config.ssl.key.startsWith('/') 
                ? config.ssl.key : service.getDataPath(config.ssl.key), true);
            cert = fsys.readTextFile(config.ssl.cert.startsWith('/')
                ? config.ssl.cert : service.getDataPath(config.ssl.cert), true);
        }
        const server = new Server(config.server ? config.server.port || 8002 : 8002, 
            key && cert ? {key, cert} : {});

        // web resources
        server.all(['get', 'head'], (req, res) => {
            const pathname = new URL(req.url).pathname;
            const respath = service.getClientPath(pathname);
            return respath 
                ? sendResource(res, respath, req.method === 'HEAD')
                : sendJson(res, {ok: false, message: `No resource at ${req.url}`}, 404);
        });

        // feed
        server.get('/feed', (req, res) => {
            return doFeed('GET', req, res);
        });
        server.get('/feed-infos', (req, res) => {
            return doFeed('HEAD', req, res);
        });
        
        // beacon: stop
        // ------------------------------------------------
        server.post('/stop', (_req, res) => {
            server.stop();
            return Promise.resolve(sendJson(res, {}));
        });

        // get or save feeds content
        // -------------------------
        server.path('/feeds')
            .get(async (_req, res) => {
                return sendResponse(res, service.getFeeds());
            })
            .post(async (req, res) => {
                const body = await req.text();
                if (body) {
                    try {
                        service.setFeeds(body);
                        logger.info('Feeds saved');
                        return sendJson(res, {ok: true, message: 'feeds saved'}, 200);
                    } catch(err) {
                        logger.error(`Error while saving feeds: ${err}`);
                        return sendJson(res, {ok: false, message: err}, 500);
                    }
                } else {
                    return sendJson(res, {ok: false, message: 'Wrong data in body'}, 400);
                }
            });
        
        // bookmarks
        server.path('/bookmarks')
            .get(async (_req, res) => {
                return sendResponse(res, service.getBookmarks());
            })
            .post(async (req, res) => {
                const body = await req.text();
                try {
                    service.setBookmarks(body);
                    return sendJson(res, {ok: true, message: 'bookmarks saved'}, 200);
                } catch(err) {
                    logger.error(`Error while saving bookmarks: ${err}`);
                    return sendJson(res, {ok: false, message: err}, 500);
                }
            });
        
        // opml
        server.path('/opml')
            .get(async (_req, res) => {
                return sendResponse(res, /**@type {string}*/(service.getOpml()));
            })
            .post(async (req, res) => {
                const body = await req.text();
                if (body) {
                    try {
                        service.setOpml(body);
                        logger.info('Opml saved');
                        return sendJson(res, {ok: true, message: 'opml saved'}, 200);
                    } catch(err) {
                        logger.error(`Error while saving opml: ${err}`);
                        return sendJson(res, {ok: false, message: err}, 500);
                    }
                }
                return sendJson(res, {ok: false, message: 'Wrong data in body'}, 400);
            });
        
        // logs
        server.path('/logs')
            .get((_req, res) => {
                try {
                    return sendResponse(res, logger.read().join('\n'));
                } catch(error) {
                    return sendJson(res, {ok:false, message: error.toString()}, 500);
                }
            })
            .post(async (req, res) => {
                const body = /**@type {{level:String, message: string}}*/ (await req.json());
                if (body && body.level && body.message) {
                    try {
                        logger.write(body.level, body.message);
                        return sendJson(res, {ok: true, message: 'Log added'});
                    } catch(error) {
                        return sendJson(res, {ok: false, message: error.message}, 500);
                    }
                }
                return sendJson(res, {ok: false, message: 'No data in body'}, 400);
            })
            .delete((_req, res) => {
                try {
                    logger.clear();
                    logger.info('Logs cleared');
                    return sendJson(res, {ok: true, message: 'Logs cleared'});
                } catch(error) {
                    logger.error(error.message);
                    return sendJson(res, {ok: false, message: error.message});
                }
            });
    },
};
