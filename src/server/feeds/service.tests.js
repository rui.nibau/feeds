console.group('server - service');

const _urlOmacronides = 'https://omacronides.com/feed';
const _urlArtStation = 'https://somartist.artstation.com/rss';
const _urlTumblr = 'https://70sscifiart.tumblr.com/rss';

const headers = {
    'Accept': 'application/xml, application/rss+xml, application/atom+xml, application/rdf+xml;q=0.8, text/xml;q=0.4', 
    'Accept-Encoding': 'gzip,inflate,br',
    // 'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:10.0) Gecko/20100101 Firefox/133.0',
};

const requestFeed = (method, url) => {
    const time = Date.now();
    // XXX abort of 5000 ms : a lot of feeds request are on error
    return globalThis.fetch(_urlArtStation, {
        method: method, 
        headers, 
        signal: AbortSignal.timeout(15000),
        cache: 'no-store',
        credentials: 'include',
    }).then(async response => {
        if (response.ok) {
            const headers = {};
            response.headers.forEach((value, name) => headers[name] = value);
            const body = method === 'GET' ? await response.text() : null;
            console.log(`Feed: ${method} ${url} in ${(Date.now() - time)/1000} s`);
            return {headers, body};
        }
        if ((response.status === 301 || response.status === 302) && response.headers.has('location')) {
            const u = new URL(url);
            const loc = response.headers.get('location');
            const newUrl = loc.startsWith('/') ? `${u.protocol}//${u.host}/${loc}` : loc;
            console.log(`Feed change: from <${url}> to <${newUrl}>`);
            return requestFeed(method, newUrl);
        }
        console.log(response.headers);
        const error = new Error(method === 'GET' ? (await response.text()) : response.statusText);
        error.name = 'FeedError';
        throw error;
    }).catch(error => {
        console.log(`Feed: ${method} ${url} - ${error.toString()}`);
        throw error;
    });
};

requestFeed('GET', _urlArtStation).then(({headers}) => {
    console.log(headers);
    // console.log(body);
}).catch(_err => {
    console.error('ERROR');
});

console.groupEnd();