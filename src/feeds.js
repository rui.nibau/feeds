import main from './server/feeds/main.js';

let action = null;
if (typeof Deno === 'object') {
    action = Deno.args[0];
} else {
    const { argv } = await import('node:process');
    action = argv[2];
}
// run
if (action) {
    if (action === 'start') {
        main.start();
    } else {
        console.warn('Wrong action');
    }
} else {
    console.warn('Wrong parameters');
}
