Feeds, a simple feed reader in javascript that can run on node.js and Deno

• [Website](https://omacronides.com/projets/feeds)
• [Sources](https://framagit.org/rui.nibau/feeds)
• [Issues](https://framagit.org/rui.nibau/feeds/-/issues)
• [Download](https://omacronides.com/lab/feeds/build/latest/feeds.zip)


