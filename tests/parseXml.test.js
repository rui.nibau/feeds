import { parseXml } from '../src/frontend/feeds/utils.js';
import dsys from '../src/server/rnb/lib/runtime/dsys.js';

const body = `<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <title>Feed channel title</title>
        <link>https://foo.bar.com</link>
        <description>Loerm ipsum.</description>
        <lastBuildDate>Fri, 15 Sep 2023 23:00:22 GMT</lastBuildDate>
        <item>
            <title>This is the item title</title>
            <link>https://foo.bar.com/baz/</link>
            <category>TV</category>
            <category>Star Trek</category>
            <category>star trek: voyager</category>
            <description>This is the description</description>
            <content>
                <style>body {background:red;}</style>
                <p onclick="foo()">This is <strong>the content</strong>.</p>
                <iframe href="foo.com"></iframe>
                <script>
                    alert("foo");
                </script>
            </content>
            <pubDate>Fri, 15 Sep 2023 23:00:22 GMT</pubDate>
            <guid>https://foo.bar.com/baz/</guid>
        </item>
    </channel>
</rss>
`;

await dsys();

(() => {
    console.group('parseXML');
    const doc = parseXml(body);
    console.assert(doc.querySelectorAll('script').length === 0, 'Fail to clean script elements');
    console.assert(doc.querySelectorAll('style').length === 0, 'Fail to clean style elements');
    console.assert(doc.querySelectorAll('iframe').length === 0, 'Fail to clean iframe elements');
    console.assert(doc.querySelector('p').onclick === undefined, 'Fail to clean event handlers attributes');

    // console.log((new globalThis.XMLSerializer()).serializeToString(doc));
    console.groupEnd();
})();
