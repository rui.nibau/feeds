const doc = new globalThis.DOMParser().parseFromString(`<?xml version="1.0" encoding="UTF-8"?>
<rss version="2.0" xmlns:content="http://purl.org/rss/1.0/modules/content/">
    <channel>
        <title>Feed channel title</title>
        <link>https://foo.bar.com </link>
        <description>Loerm ipsum.</description>
        <lastBuildDate>Fri, 15 Sep 2023 23:00:22 GMT</lastBuildDate>
        <item>
            <title><![CDATA[This is the item title]]></title>
            <link><![CDATA[https://foo.bar.com/baz/]]></link>
            <category><![CDATA[TV]]></category>
            <category><![CDATA[Star Trek]]></category>
            <category><![CDATA[star trek: voyager]]></category>
            <description><![CDATA[This is the description]]></description>
            <content:encoded><![CDATA[<p>This is <strong>the content</strong>.</p>]]></content:encoded>
            <pubDate>Fri, 15 Sep 2023 23:00:22 GMT</pubDate>
            <guid>https://foo.bar.com/baz/</guid>
        </item>
    </channel>
</rss>`, 'application/xml');
console.log(doc);
const els = doc.getElementsByTagName('content:encoded');
const el = els.length > 0 ? els[0] : null;
console.log(els);
if (el) {
    if (el.getAttribute('type') === 'html') {
        console.log(el.textContent);
    } else if (el.childElementCount > 0) {
        for (const child of el.children) {
            console.log(child);
        }
    } else if (el.childNodes.length > 0) {
        for (const childNode of el.childNodes) {
            // eslint-disable-next-line no-undef
            if (childNode instanceof CDATASection) {
                console.log('CDATASection');
                console.log(childNode.data);
            } else {
                console.log(childNode.textContent);
            }
        }
    }
}
