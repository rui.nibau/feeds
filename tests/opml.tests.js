import fs from 'node:fs';
import path from 'node:path';
import { fileURLToPath } from 'node:url';

import opml from '../src/frontend/rnb/lib/ui/opml/HTMLOpml.js';

const currentPath = path.dirname(fileURLToPath(import.meta.url));

const opmlLoad = async path => {
    const content = fs.readFileSync(path, 'utf-8');
    return opml.parse(content);
};

export default (async () => {
    const doc = await opmlLoad(`${currentPath}/../src/data/feeds.opml`);
    // const doc = await opml.load('./../src/data/feeds.opml');
    console.log(doc);
})();