console.log(import.meta.url);

const findBasePath = async () => {
    let BASE_PATH = '';
    if ('resolve' in import.meta) {
        console.log('on Deno');
        BASE_PATH = new URL(import.meta.resolve('./../')).pathname;
    } else {
        console.log('on nodejs');
        const { fileURLToPath } = await import('node:url');
        const path = await import('node:path');
        BASE_PATH = path.join(path.dirname(fileURLToPath(import.meta.url)), '/../');
    }
    console.log(BASE_PATH);
};

console.log(new globalThis.CustomEvent('foo'));
